// Теоретичні питання

// 1.Опишіть, як можна створити новий HTML тег на сторінці.
//Створити елемент, записати його в змінну, вказавши потрібний тег const elem = document.createElement('div'); додати єлемент, вказавши місце його додавання, та де саме він буде доданий(напочатку, вкінці) document.body.append(elem)
// 2.Опишіть, що означає перший параметр функції insertAdjacentHTML і опишіть можливі варіанти цього параметра.
//Означає місце, де буде знаходитись елемент(перед відкриваючим тегом, після вікриваючого тега, перед закриваючим тегом, після закриваючого тегу) : beforebegin, afterbegin, beforeend, afterend
// 3.Як можна видалити елемент зі сторінки?
//Метод Element.remove() видаляє елемент 

// Завдання
// Реалізувати функцію, яка отримуватиме масив елементів і виводити їх на сторінку у вигляді списку. Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// Технічні вимоги:
// Створити функцію, яка прийматиме на вхід масив і опціональний другий аргумент parent - DOM-елемент, до якого буде прикріплений список (по дефолту має бути document.body.
// кожен із елементів масиву вивести на сторінку у вигляді пункту списку;
// Приклади масивів, які можна виводити на екран:

// ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
// ["1", "2", "3", "sea", "user", 23];
// Можна взяти будь-який інший масив.
// Необов'язкове завдання підвищеної складності:
// Додайте обробку вкладених масивів. Якщо всередині масиву одним із елементів буде ще один масив, виводити його як вкладений список. Приклад такого масиву:

// ["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"];
// Підказка: використовуйте map для обходу масиву та рекурсію, щоб обробити вкладені масиви.
// Очистити сторінку через 3 секунди. Показувати таймер зворотного відліку (лише секунди) перед очищенням сторінки.



const myArr = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
const myArr2 = ["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"];

const bodyElem = document.querySelector('body')

function createList (arr, elem){
    const newList = document.createElement('ul');
    
    arr.forEach((newListItem)=>{
        
    elem.append(newList);
    const item = document.createElement('li');

        if (!Array.isArray(newListItem)){
            
            newList.append(item);
            item.innerText = newListItem;
            
        
        }else if(Array.isArray(newListItem)){
           
            createList(newListItem, newList);
            
        }
        
    })
    
}

function deleteList() {
    const removeList = document.querySelectorAll('ul');
    removeList.forEach((removeList)=>{
       removeList.remove();
    })
    
    
}
function timer() {
    let timer = 3; 
    const secondsTimer = document.createElement('span');
    secondsTimer.innerText = timer;
    bodyElem.append(secondsTimer);
    
        
    const time = setInterval(() => {
        timer --;
        secondsTimer.innerText = timer;
        
    }, 1000)
    
    

    setTimeout(()=> {
        if (timer === 0){
            
            secondsTimer.innerText = 'time is up';
        }
        clearInterval(time);
    }, 3000)

    setTimeout(()=> {
        if (timer === 1){
            
            secondsTimer.innerText = 'time is up';
        }
        clearInterval(time);
    }, 3000)
}

createList(myArr, bodyElem)
createList(myArr2, bodyElem)
timer()
setTimeout(deleteList, 3000);